const limitFunctionCallCount = (cb,n) => {
    let count = n;
    /*
    if (!typeof(n) === Number){
        count = 0;
    }
*/
    const invokeCb = (...arg) => {

        if (count > 0){
            --count;
            return cb(...arg);
        }
        else{
            return null;
        }
    }
    return invokeCb;
}


module.exports = limitFunctionCallCount;