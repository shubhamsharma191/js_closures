const cacheFunction = (cb) =>{

    let cache = {};

    const invokeCb = (...arg) => {

        let temp1 = JSON.stringify(arg)

        if(cache.hasOwnProperty(temp1)){

            return cache[temp1];
        }

        else{
            //console.log("new")

            let temp2 = cb(...arg);
            cache[temp1] = temp2

            return temp2
        }

    }
    return invokeCb;

} 
module.exports = cacheFunction;