 const counterFactory = () => {

    let count = 0;

    let obj = { increment : () => {

        return ++count;
    } ,

    decrement : () => {

        return --count;
    }
    }
    return  obj
 }

module.exports = counterFactory;